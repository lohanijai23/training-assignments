﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASPMVCDemo.Custom_Validators
{
    public class MyCustomValidator:ValidationAttribute
    {
        private readonly int _maxWords;

        public MyCustomValidator(int m)
        {
            _maxWords = m;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var errorMsg = "ERROR";
            if (value==null)
            {

                return  new ValidationResult(errorMsg);
            }
            Console.WriteLine("inside isvalid");
            var userIp = Convert.ToString(value);

            if (userIp.Split(' ').Length <= _maxWords)
                return ValidationResult.Success;

              errorMsg = FormatErrorMessage((validationContext.DisplayName));
            
                return new ValidationResult(errorMsg);


        }
    }
}