﻿using ASPMVCDemo.Custom_Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASPMVCDemo.Models
{
    public class EmpDetails
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Required]
        public int Age { get; set; }

        [Required][MaxLength(20)]
        public string Name { get; set; }

        [MyCustomValidator(2)]
        public string Department { get; set; }


    }
}