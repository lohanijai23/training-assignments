﻿using ASPMVCDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPMVCDemo.Controllers
{
    public class EmpController : Controller
    {
        // GET: Emp
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Add()
        {
            return View();
        }

       [HttpGet]
        public ActionResult CreateEmp()
        {

                return View();
        }


        [HttpPost]
        public ActionResult CreateEmp(EmpDetails empDetails)
        {

            if (!ModelState.IsValid)
            {
               // System.Web.Mvc.Html.ValidationMessageFo.;
            }

            string name = empDetails.Name;
            int age = empDetails.Age;
            string dept = empDetails.Department;


             return Content(name+" "+age+ " "+ dept);
           
        }


        public ActionResult ViewEmp()
        {
            List<EmpDetails> empList = new List<EmpDetails> { new EmpDetails {Name="Jai", ID=1, Age=23,Department="comp" },
                                                             new EmpDetails {Name="Rao", ID=2, Age=22,Department="comp" },
                                                                new EmpDetails {Name="Lahiri", ID=3, Age=24,Department="IT" },
                                                                 new EmpDetails {Name="Mohit", ID=4, Age=22,Department="comp" },
                                                                   new EmpDetails {Name="akhilesh", ID=5, Age=21,Department="IT" }};
            

            return View(empList);
        }

    }
}