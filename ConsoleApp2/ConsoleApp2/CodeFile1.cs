﻿/*Marks in Maths >=65
Marks in Phy >=55
Marks in Chem>=50
Total in all three subject >=180
or
Total in Math and Subjects >=140
Accept the marks as input.Also display the average(do not round off).

*/


using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine("Enter Marks in Maths: ");
            int mm = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter Marks in Physics: ");
            int pm = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter Marks in Chemistry: ");
            int cm = int.Parse(Console.ReadLine());

            float avg = (mm + pm + cm) / 3;

           Console.WriteLine("Average Marks: ",avg);

            if (((mm>=65 && pm>=55 &&cm>=50 )  &&(mm + pm + cm > 180 ))|| (mm > 70 && mm + pm + cm > 140))
                Console.WriteLine("Congrats! You're Eligible for Admission");
            else
                Console.WriteLine("Sorry! You're not Eligible for Admission");

            Console.ReadKey();
        }

    }
}
