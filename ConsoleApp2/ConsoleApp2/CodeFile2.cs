﻿/*
 3)	Write a program to find first 10 multiples of x and y.Find the sum of multiples of x, store it in firstSumOfMultiple.Find the sum of multiples of y, store it in secondSumOfMultiple.
If firstSumOfMultiple is greater than secondSumOfMultiple then perform
firstSumOfMultiple /secondSumOfMultiple, else perform secondSumOfMultiple /firstSumOfMultiple.
Accept x and y as user input, write the output on console.


using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine("Enter x: ");
            int x = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter y: ");
            int y = int.Parse(Console.ReadLine());


            int firstSumofMultiple = 0;
            int secondSumofMultiple = 0;


            for(int i=1; i<=10; i++)
            {
                firstSumofMultiple += x * i;
                secondSumofMultiple += y * i;
            }


            Console.WriteLine("Sum of first 10 multiples of "+x+": "+firstSumofMultiple);
            Console.WriteLine("Sum of first 10 multiples of " + y + ": " + secondSumofMultiple);

           


           if (firstSumofMultiple>=secondSumofMultiple)
            {
                Console.WriteLine(firstSumofMultiple / secondSumofMultiple);
            }

           else
                Console.WriteLine(secondSumofMultiple/firstSumofMultiple );

            Console.ReadKey();
        }

    }
}


*/