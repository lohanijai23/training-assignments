﻿using MvcApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Net.Http.Formatting;

namespace MvcApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult empView()
        {
            IEnumerable<Employee> emp = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:56093");
                //HTTP GET
                var responseTask = client.GetAsync("/api/Employees");
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<Employee>>();
                    readTask.Wait();

                    emp = readTask.Result;
                }
                else //web api sent error response 
                {
                    //log response status here..

                    emp = Enumerable.Empty<Employee>();

                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return View(emp);
        }

        public ActionResult empCreate()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult empCreate(Employee emp)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:56093/api/Employees");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<Employee>("Employees", emp);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("empView");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(emp);

          
        }

       
        public ActionResult empEdit(int id)
        {
            Employee emp = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:56093/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Employees?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Employee>();
                    readTask.Wait();

                    emp = readTask.Result;
                }
            }
            return View(emp);

           
        }

        [HttpPost]
        public ActionResult empEdit(int id, Employee emp)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:56093/api/");

                //HTTP PUT
                var putTask = client.PutAsJsonAsync<Employee>("Employees/"+  id, emp);
                putTask.Wait();

                var result = putTask.Result;
                if (result.IsSuccessStatusCode)
                {

                    return RedirectToAction("empView");
                }
            }
            // return View(emp);
            return RedirectToAction("empView");
        }

        [HttpGet]
        public ActionResult empDelete(int id)
        {

            Employee emp = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:56093/api/");
                //HTTP GET
                var responseTask = client.GetAsync("Employees?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<Employee>();
                    readTask.Wait();

                    emp = readTask.Result;
                }
            }
            return View(emp);

        }

        [HttpPost]
        public ActionResult empDelete(Employee emp, int id)
        {
            

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:56093/api/");
                //HTTP GET
                var responseTask = client.DeleteAsync("Employees?id=" + id.ToString());
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction("empView");
                }
            }

            return RedirectToAction("empView");
        }






    }
}