﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace two_assignments
{
    class QuestionPaper
    {

        List<string> SetA = new List<string>();
        List<string> SetB = new List<string>();
        List<string> SetC = new List<string>();
        List<string> SetD = new List<string> ();

        List<string> QPaper = new List<string>();
        int TotalQuestions { get; set; }

        public QuestionPaper()
        {
            for (int i = 0; i < 18000; i++)
            {
                SetC.Add("C" + i);
            }
            for(int i=0; i<12000; i++)
            {
                SetD.Add("D" + i);
            }
            for (int i = 0; i < 10000; i++)
            {
                SetA.Add("A" + i );
                SetB.Add("B" + i );
            }

            TotalQuestions = SetA.Count + SetB.Count + SetC.Count + SetD.Count;
        }
        

        public void SetPaper()
        {
          
          
            

            int []Count = new int[4];
            Count[0]=(SetA.Count);
            Count[1]=(SetB.Count);
            Count[2]=(SetC.Count);
            Count[3]=(SetD.Count);

            int a = 0, b = 0, c = 0, d = 0;
            int max = RetMax(Count, -1);

            for (int i = 0; i < TotalQuestions; i++)
            {
               
                
                if(max==0 )
                {
                  
                    
                        QPaper.Add(SetA[a]);
                        a++;
                        Count[0]--;
                    

                    max = RetMax(Count, 0);

               }
                else if(max==1 )
                {
                    QPaper.Add(SetB[b]);
                    b++;
                    Count[1]--;
                    max = RetMax(Count, 1);
                }
                else if (max == 2 )
                {
                    QPaper.Add(SetC[c]);
                    c++;
                    Count[2]--;
                    max = RetMax(Count, 2);
                }
                else if (max == 3 )
                {
                    QPaper.Add(SetD[d]);
                    d++;
                    Count[3]--;
                    max = RetMax(Count, 3);
                }
            }

        }

        public int RetMax(int[] c, int i)
        {
            int max=0;

            if (i==-1)
                max= Math.Max(Math.Max(c[0],c[1]), Math.Max(c[2], c[3] ));
            else if(i==0)
                max= Math.Max(c[1], Math.Max(c[2], c[3]));
            else if (i == 1)
                max = Math.Max(c[0], Math.Max(c[2], c[3]));
            else if (i == 2)
                max = Math.Max(Math.Max(c[0], c[1]), c[3]);
            else if (i == 3)
                max = Math.Max(Math.Max(c[0], c[1]), c[2]);


            if (max == c[0])
                return 0;
            else if (max == c[1])
                return 1;
            else if (max == c[2])
                return 2;
            else if (max == c[3])
                return 3;

            return 0;


        }

        public void Display()
        {
            Console.WriteLine("Question Paper");
            for (int i = 0; i < QPaper.Count; i++)
            {
                Console.Write(QPaper[i]+" ");
            }

            Console.WriteLine(QPaper.Count);
        }


    }
}
