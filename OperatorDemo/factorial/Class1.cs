﻿using System;

namespace Factorial
{
    public class factorial
    {
        public delegate void SendResult(int n);
        public  static event SendResult x;

        public static void fact(int n)
        {
            int f = 1;
            for (int i = n; i > 1; i--)
                f = f * i;

            x(f);
        }
    }
}
