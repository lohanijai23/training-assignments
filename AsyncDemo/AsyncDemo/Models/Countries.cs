﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncDemo.Models
{
    public class Countries
    {
        public string Name { get; set; }

        public Countries()
        {

        }
        public Countries(string n)
        {
            Name = n;
        }


        public async Task<Countries> getCountries(string N)
        {
            await Task.Delay(2);
            Countries obj = new Countries(N);
            return obj;
        }


        public List<Countries> synclist()
        {
            List<Countries> l = new List<Countries>();
            for (int i = 0; i < 5000; i++)
            {
                Thread.Sleep(2);
                Countries c = new Countries("Country" + i);
                c.Name = "Country" + i;
                l.Add(c);
            }

            return l;

        }

        public async Task<List<Countries>> asynclist()
        {

            List<Countries> l = new List<Countries>();

            Countries[] arr = new Countries[5000];

            for (int i = 0; i < 5000; i++)
            {

                arr[i] = new Countries();
                arr[i] = await getCountries("Country " + i);
                
            }

            l = arr.ToList();

            return l;


        }

    }
}
