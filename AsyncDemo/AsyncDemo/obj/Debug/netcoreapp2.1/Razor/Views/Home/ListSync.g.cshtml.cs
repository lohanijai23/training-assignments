#pragma checksum "C:\Users\lohani_j\source\repos\AsyncDemo\AsyncDemo\Views\Home\ListSync.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8940f1a7250adf32de9c94d336315dca6ea97351"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_ListSync), @"mvc.1.0.view", @"/Views/Home/ListSync.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/ListSync.cshtml", typeof(AspNetCore.Views_Home_ListSync))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\lohani_j\source\repos\AsyncDemo\AsyncDemo\Views\_ViewImports.cshtml"
using AsyncDemo;

#line default
#line hidden
#line 2 "C:\Users\lohani_j\source\repos\AsyncDemo\AsyncDemo\Views\_ViewImports.cshtml"
using AsyncDemo.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8940f1a7250adf32de9c94d336315dca6ea97351", @"/Views/Home/ListSync.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e656b861323599807ef3b15b59b361a90c55fcd3", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_ListSync : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<AsyncDemo.Models.Countries>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(48, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\lohani_j\source\repos\AsyncDemo\AsyncDemo\Views\Home\ListSync.cshtml"
  
    ViewData["Title"] = "ListSync";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
            BeginContext(141, 128, true);
            WriteLiteral("\r\n<h2>Syncronously Fetching from list</h2>\r\n\r\n\r\n<div>\r\n    <br />\r\n    <label class=\"btn btn-primary\"> Excecution Time</label>\r\n");
            EndContext();
#line 14 "C:\Users\lohani_j\source\repos\AsyncDemo\AsyncDemo\Views\Home\ListSync.cshtml"
     if (ViewBag.WatchMilliseconds != null)
    {

#line default
#line hidden
            BeginContext(321, 39, true);
            WriteLiteral("        <h1 class=\"label label-danger\">");
            EndContext();
            BeginContext(361, 25, false);
#line 16 "C:\Users\lohani_j\source\repos\AsyncDemo\AsyncDemo\Views\Home\ListSync.cshtml"
                                  Write(ViewBag.WatchMilliseconds);

#line default
#line hidden
            EndContext();
            BeginContext(386, 19, true);
            WriteLiteral(" millisecond</h1>\r\n");
            EndContext();
#line 17 "C:\Users\lohani_j\source\repos\AsyncDemo\AsyncDemo\Views\Home\ListSync.cshtml"
    }

#line default
#line hidden
            BeginContext(412, 18, true);
            WriteLiteral("\r\n</div>\r\n\r\n<ol>\r\n");
            EndContext();
#line 22 "C:\Users\lohani_j\source\repos\AsyncDemo\AsyncDemo\Views\Home\ListSync.cshtml"
     foreach (var item in Model)
    {

#line default
#line hidden
            BeginContext(471, 12, true);
            WriteLiteral("        <li>");
            EndContext();
            BeginContext(484, 9, false);
#line 24 "C:\Users\lohani_j\source\repos\AsyncDemo\AsyncDemo\Views\Home\ListSync.cshtml"
       Write(item.Name);

#line default
#line hidden
            EndContext();
            BeginContext(493, 7, true);
            WriteLiteral("</li>\r\n");
            EndContext();
#line 25 "C:\Users\lohani_j\source\repos\AsyncDemo\AsyncDemo\Views\Home\ListSync.cshtml"
    }

#line default
#line hidden
            BeginContext(507, 9, true);
            WriteLiteral("</ol>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<AsyncDemo.Models.Countries>> Html { get; private set; }
    }
}
#pragma warning restore 1591
