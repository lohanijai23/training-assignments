﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeData
{
    public class EmpClass
    {
        public int ID { get; set; }
        public int Salary { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }

       public static void Create_Emp(string n, string d, int s)
        {
            Employee e = new Employee
            {
                Name = n,
                department = d,
                salary = s
            };

            using (var dbCtx = new WcfDBEntities())
            {
                
                dbCtx.Employees.Add(e);

                dbCtx.SaveChanges();
            }

        }


       public static void Update_Emp(int id, string n, string d, int s)
        {
            using (var dbCtx = new WcfDBEntities())
            {
                var emp = (from e in dbCtx.Employees where e.Id == id select e).Single();
                emp.Name = n;
                emp.salary = s;
                emp.department = d;

                dbCtx.SaveChanges();
            }

        }

       public static void Delete_Emp(int id)
        {
            using (var dbCtx = new WcfDBEntities())
            {
                var emp = (from e in dbCtx.Employees where e.Id == id select e).Single();

                dbCtx.Employees.Remove(emp);
                dbCtx.SaveChanges();
            }

        }


        public static EmpClass Get_Employee(int id)
        {
            using (var dbCtx = new WcfDBEntities())
            {
                var emp = (from e in dbCtx.Employees where e.Id == id select e).Single();


                EmpClass em = new EmpClass { ID = emp.Id, Name = emp.Name, Salary = Convert.ToInt32(emp.salary), Department = emp.department };
                return em;
            }
        }

    }
}
