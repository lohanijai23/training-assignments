﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using EmployeeData;

namespace EmployeeService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.

    [ServiceBehavior (InstanceContextMode =InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class Service1 : IService1
    {
        public void CreateEmp(string name, int salary, string dept)
        {
            EmpClass.Create_Emp(name, dept, salary);
            
        }

        public void DeleteEmp(int id)
        {
            EmpClass.Delete_Emp(id);
        }

     

        public WcfEmployee GetEmp(int id)
        {
            
            EmpClass e= EmpClass.Get_Employee(id);

            WcfEmployee wemp = new WcfEmployee
            {
                ID = e.ID,
                Name = e.Name,
                Salary = e.Salary,
                Department = e.Department
            };

            return wemp;

        }

        public void UpdateEmp(int id, string name, int salary, string dept)
        {
            EmpClass.Update_Emp(id, name, dept, salary);
        }
    }
}
