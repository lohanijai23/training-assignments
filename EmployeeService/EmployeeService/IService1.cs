﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EmployeeService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract(IsOneWay =true)]
        void CreateEmp(string name, int salary, string dept);

        [OperationContract]
        WcfEmployee GetEmp(int id);


        [OperationContract(IsOneWay = true)]
        void UpdateEmp(int id, string name, int salary, string dept);


        [OperationContract(IsOneWay = true)]
        void DeleteEmp(int id);

        // TODO: Add your service operations here
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "EmployeeService.ContractType".
    [DataContract]
    public class WcfEmployee
    {

        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int Salary { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Department { get; set; }
    }
}
