﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQSample
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> l = new List<string> { "apple", "ball", "cat", "dog" };


            var st = from word in l where word == "ball" select word;

            Console.WriteLine(st);


            studentService1.StudentClient client = new studentService1.StudentClient();

            foreach (studentService1.Student s in client.GetAllStudent())
            {
                Console.WriteLine(s.Id + " " + s.Name);
            }
            Console.ReadLine();

        }
    }
}
