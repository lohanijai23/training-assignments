﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{   
    
    class Program
    {
        /// <summary>
        /// this class prints colors
        /// </summary>
        static void Main(string[] args)
        {
            for (int i=1; i<=100; i++)
            {
                if (i % 2 == 0 && i % 3 == 0 && i % 5 == 0)
                    Console.WriteLine(i);

            }

            for(int i = 111; i <= 900; i++)
            {
                int f = 0;
                for(int j=2; j<Math.Sqrt(i);j++)
                {
                    if (i % j == 0)
                    {
                        f = 1;
                        break;
                    }

                    
                }
                if (f == 0)
                    Console.WriteLine(i);
            }
            Console.ReadLine();
        }
    }
}
