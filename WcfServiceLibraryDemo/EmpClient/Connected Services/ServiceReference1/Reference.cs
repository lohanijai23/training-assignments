﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EmpClient.ServiceReference1 {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference1.IEmployee")]
    public interface IEmployee {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IEmployee/GetEmployee", ReplyAction="http://tempuri.org/IEmployee/GetEmployeeResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WcfServiceLibraryDemo_Employee.myerror), Action="http://tempuri.org/IEmployee/GetEmployeemyerrorFault", Name="myerror", Namespace="http://schemas.datacontract.org/2004/07/WcfServiceLibraryDemo_Employee")]
        string GetEmployee(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IEmployee/GetEmployee", ReplyAction="http://tempuri.org/IEmployee/GetEmployeeResponse")]
        System.Threading.Tasks.Task<string> GetEmployeeAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IEmployee/Greet", ReplyAction="http://tempuri.org/IEmployee/GreetResponse")]
        EmpClient.ServiceReference1.response Greet(EmpClient.ServiceReference1.authenticate request);
        
        // CODEGEN: Generating message contract since the operation has multiple return values.
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IEmployee/Greet", ReplyAction="http://tempuri.org/IEmployee/GreetResponse")]
        System.Threading.Tasks.Task<EmpClient.ServiceReference1.response> GreetAsync(EmpClient.ServiceReference1.authenticate request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="authenticate", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class authenticate {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://tempuri.org/")]
        public string Password;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public WcfServiceLibraryDemo_Employee.Employee emp;
        
        public authenticate() {
        }
        
        public authenticate(string Password, WcfServiceLibraryDemo_Employee.Employee emp) {
            this.Password = Password;
            this.emp = emp;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="response", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class response {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public WcfServiceLibraryDemo_Employee.Employee emp;
        
        public response() {
        }
        
        public response(WcfServiceLibraryDemo_Employee.Employee emp) {
            this.emp = emp;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IEmployeeChannel : EmpClient.ServiceReference1.IEmployee, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class EmployeeClient : System.ServiceModel.ClientBase<EmpClient.ServiceReference1.IEmployee>, EmpClient.ServiceReference1.IEmployee {
        
        public EmployeeClient() {
        }
        
        public EmployeeClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public EmployeeClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public EmployeeClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public EmployeeClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string GetEmployee(int id) {
            return base.Channel.GetEmployee(id);
        }
        
        public System.Threading.Tasks.Task<string> GetEmployeeAsync(int id) {
            return base.Channel.GetEmployeeAsync(id);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        EmpClient.ServiceReference1.response EmpClient.ServiceReference1.IEmployee.Greet(EmpClient.ServiceReference1.authenticate request) {
            return base.Channel.Greet(request);
        }
        
        public void Greet(string Password, ref WcfServiceLibraryDemo_Employee.Employee emp) {
            EmpClient.ServiceReference1.authenticate inValue = new EmpClient.ServiceReference1.authenticate();
            inValue.Password = Password;
            inValue.emp = emp;
            EmpClient.ServiceReference1.response retVal = ((EmpClient.ServiceReference1.IEmployee)(this)).Greet(inValue);
            emp = retVal.emp;
        }
        
        public System.Threading.Tasks.Task<EmpClient.ServiceReference1.response> GreetAsync(EmpClient.ServiceReference1.authenticate request) {
            return base.Channel.GreetAsync(request);
        }
    }
}
