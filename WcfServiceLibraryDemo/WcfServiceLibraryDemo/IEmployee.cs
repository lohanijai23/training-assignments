﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WcfServiceLibraryDemo_Employee
{
    [ServiceContract]
    public interface IEmployee
    {
        [OperationContract]
        [FaultContract(typeof(myerror))]
        string GetEmployee(int id);

        [OperationContract]


        //response Greet(authenticate obj);

        string Greet(authenticate obj);
    }

    [DataContract]

    public class Employee
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
    }

    [DataContract]
    public class myerror
    {
        public string emsg { get; set; }
        public myerror(string em)
        {
            emsg = em;
        }
    }


    [MessageContract]
    //[KnownType(typeof(Employee))]

    public class authenticate
    {
        [MessageHeader]
        public string Password { get; set; }
        [MessageBodyMember]
        public Employee emp { get; set; }


    }

    [MessageContract]
    //[KnownType(typeof(Employee))]
    public class response
    {
        [MessageBodyMember]
        public Employee emp { get; set; }

    }


}
