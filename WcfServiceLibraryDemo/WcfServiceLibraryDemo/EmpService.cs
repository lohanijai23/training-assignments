﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WcfServiceLibraryDemo_Employee
{
    public class EmpService : IEmployee
    {
        public string GetEmployee(int id)
        {
            List<Employee> emp = new List<Employee> { new Employee { Id = 1, Name = "Jai Lohani" },

            new Employee { Id = 2, Name = "Aditya Rao" },
            new Employee { Id = 3, Name = "Praveen" },
            new Employee { Id = 4, Name = "Anmol" },
            new Employee { Id = 5, Name = "Raj" }};

            if (id < 1 || id > 5)
            {
                myerror err = new myerror("Invalid ID!!");
                throw new FaultException<myerror>(err, "Invalid ID!!");
            }

            foreach (Employee e in emp)
                if (e.Id == id)
                    return e.Name;
            return null;
        }

      

        //public response Greet(authenticate obj)
        //{
        //    if(obj.Password!="root")
        //    {
        //        myerror err = new myerror("");
        //        throw new FaultException<myerror>(err, "Invalid Password!!");

               
        //    }
        //    else
        //    {
        //        response r = new response
        //        {
        //            emp = obj.emp
        //        };


        //        return r;
        //    }
            
        //}

        public string Greet(authenticate obj)
        {
            if (obj.Password != "root")
            {
                myerror err = new myerror("");
                throw new FaultException<myerror>(err, "Invalid Password!!");


            }
            else
            {
                response r = new response
                {
                    emp = obj.emp
                };
                return r.emp.Name;
                
            }

        }
    }

   
}
