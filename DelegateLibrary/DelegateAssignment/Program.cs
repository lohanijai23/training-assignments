﻿using DelegateLibrary;
using System;

namespace DelegateAssignment
{
    class Program
    {
        static void Main(string[] args)
        {

            dlib d = new dlib();

            Console.WriteLine("Enter A Number");
            int a =int.Parse(Console.ReadLine());

            Console.WriteLine("Enter Another Number");
            int b = int.Parse(Console.ReadLine());


            d.num += Add;
            Console.WriteLine("Sum : " + d.num(a, b));

            d.num -= Add;
            d.num += Mul;
            Console.WriteLine("Product : " + d.num(a, b));


            d.num -= Add;
            d.num -= Mul;
            d.num += Div;
            Console.WriteLine("Quotient : " + d.num(a, b));
            
            Console.ReadLine();
        }

       

        public static int Add(int a, int b)
        {
            return a + b;
        }

        public static int Mul(int a, int b)
        {
            return a * b;
        }

        public static int Div(int a, int b)
        {
            if (b != 0)
                return a / b;
            else
            {
                Console.WriteLine("Divisor 0!");
                return a;
            }
        }
        


        



    }
}
