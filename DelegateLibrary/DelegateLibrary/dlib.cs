﻿using System;

namespace DelegateLibrary
{
    public class dlib
    {
        public delegate double Conversion(double feet);
        public Conversion convD;

        public delegate int NumberChanger(int n, int x);
        public NumberChanger num;


        public delegate void PrintString(string s);

        public PrintString prnG;
        public PrintString prnM;

        public double convert(double n)
        {
            
            return convD(n);
        }






    }
}
