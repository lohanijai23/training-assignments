﻿using DelegateLibrary;
using System;

namespace ass3
{
    class Program
    {
        static void Main(string[] args)
        {
            dlib d = new dlib();

            Console.WriteLine("Enter A String: ");

            string s = Console.ReadLine();

            d.prnG += Good;
            d.prnM += Morning;

            d.prnG += d.prnM;


            d.prnG(s);

            Console.ReadLine();
        }

        static void Good(string s)
        {
            System.Console.WriteLine("  Good ");
        }

        static void Morning(string s)
        {
            System.Console.WriteLine("  Morning, {0}!", s);
        }
    }
}
