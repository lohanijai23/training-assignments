﻿using DelegateLibrary;
using System;

namespace celsius
{
    class Program
    {
        static void Main(string[] args)
        {
            dlib d = new dlib();

            Console.WriteLine("Enter Temperature in Farhenheit: ");
            int f = int.Parse(Console.ReadLine());

            
            Console.WriteLine( "Temperature in Celsius: " + Conv(f));

            Console.ReadLine();

        }

        static double Conv(int f)
        {
            return (Convert.ToDouble(f) - Convert.ToDouble(32)) * Convert.ToDouble(5) / Convert.ToDouble(9);
        }
    }
}
