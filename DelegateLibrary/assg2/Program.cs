﻿using DelegateLibrary;
using System;

namespace assg2
{
    class Program
    {
        static void Main(string[] args)
        {
            dlib d = new dlib();

            d.convD += retInches;

            Console.WriteLine("Enter Feet: ");

            double f = double.Parse(Console.ReadLine());



            Console.WriteLine("In Inches: " + d.convD(f) + " inches");
        }
        static double retInches(double n)
        {
            return n * 12;

        }
    }
}
