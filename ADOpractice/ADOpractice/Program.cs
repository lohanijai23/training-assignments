﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOpractice
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new SampleDbContext())
            {
                var listOfDepartments = context.Departments.ToList();

                foreach (var item in listOfDepartments)
                {
                    Console.WriteLine(item.ID);
                    Console.WriteLine(item.Name);
                    Console.WriteLine(item.Location);
                }
            }

            Console.ReadLine();
        }
    }
}
