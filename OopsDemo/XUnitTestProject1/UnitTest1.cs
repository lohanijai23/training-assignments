using CustomerMgmtBL;
using System;
using Xunit;

namespace XUnitTestProject1
{
    [TestClass]
    public class CustomerTest
    {
        [TestMethod]
        public void FullNameTestValid()
        {
            Customer cust = new Customer();
            cust.FirstName = "Bilbo";
            cust.LastName="Baggins";

            string expected = "Bilbo Baggins";

            string actual = cust.FullName;

            Assert.AreEqual(expected, actual);
        }
    }
}
