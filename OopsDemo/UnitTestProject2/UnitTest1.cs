using CustomerMgmtBL;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void FullNameTestValid()
        {
            Customer cust = new Customer();
            cust.FirstName = "Bilbo";
            cust.LastName = "Baggins";

            string expected = "Bilbo Baggins";

            string actual = cust.FullName;

            Assert.AreEqual(expected, actual);
        }

    }
}
