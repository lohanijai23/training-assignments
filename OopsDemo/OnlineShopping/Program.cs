﻿using CustomerMgmtBL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OnlineShopping
{
    class Program
    {
        static void Main(string[] args)
        {
            int ch;
            List<Customer> CList = new List<Customer>();
            List<Order> o = new List<Order>();
            List<Product> p = new List<Product>();

            for (int i = 0; i < 5; i++)
                p.Add(Product.AddToCatalog());

            
            do
            {
                Console.WriteLine("\n\n\n********Menu********\n" +
                    "\n1. New Customer" +
                    "\n2. Show Customers" +
                    "\n3. Place Orders" +
                    "\n4. View Orders\n" +
                    "\n5. Exit" +
                    "\n*********************\n");

                ch = int.Parse(Console.ReadLine());


             

                switch (ch)
                {
                    case 1:
                        {
                            
                            CList.Add(Customer.CreateCustomer());
                            
                            break;

                        }

                    case 2:
                        {
                            Console.WriteLine("\n****CUSTOMERS*****\n");
                            for (int j = 0; j < CList.Count(); j++)
                            {
                                
                                CList[j].ShowCustomers();
                            }

                            break;
                        }

                    case 3:
                        {
                            Console.WriteLine("\n\n****LOGIN******");

                            Console.WriteLine("Enter First Name: ");
                            string fn = Console.ReadLine();

                            Console.WriteLine("Enter Email ID: ");
                            string em = Console.ReadLine();

                            if(Customer.Validate(CList, fn,em)==false)
                            {
                                Console.WriteLine("Validation Failed! Login Again!\n\n");
                                break;
                            }


                            Console.WriteLine("\n\n*****Product Catalog*******\n");
                            for (int i = 0; i < p.Count; i++)
                            {
                                p[i].ShowProducts();
                            }
                            Console.WriteLine("\n*************************************\n\n ");

                            o.Add(Order.CreateOrder(p));
                            break;

                        }

                    case 4:
                        {
                            Console.WriteLine("\n\n********ORDERS***********\n");


                            for (int i = 0; i < o.Count(); i++)
                                o[i].ShowOrders();
                            Console.WriteLine("\n**********************\n\n");
                            break;
                        }

                    case 5:
                        {

                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid Choice! Please Enter Again: ");
                            break;
                        }
                }
            } while (ch >= 1 && ch <= 4);
        }
    }
}
