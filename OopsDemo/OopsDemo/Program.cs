﻿using System;
using System.Collections.Generic;

namespace OopsDemo
{
    class Program
    {
     
        static void Main(string[] args)
        {
            List<FullTimeEmp> fe = new List<FullTimeEmp> ();
            List<Contractor> con = new List<Contractor> ();

            Console.WriteLine("How Many Employees Do you Want? ");
            int NEmp = int.Parse(Console.ReadLine());

            for (int i = 0; i < NEmp; i++)
            {
              fe.Add( new FullTimeEmp());

                fe[i].Get();
                fe[i].GetEmp();
            }


            Console.WriteLine("How Many Contractors Do you Want? ");
            int NCon = int.Parse(Console.ReadLine());
            for (int i = 0; i < NCon; i++)
            {
                con.Add(new Contractor());


                con[i].Get();
                con[i].GetCon();
            }

            Console.WriteLine("\n\n-----------------\nEMPLOYEES\n----------------\n ");

            for (int i = 0; i < NEmp; i++)
            {
                fe[i].DispEmp();
            }

            Console.WriteLine("\n\n-----------------\nCONTRACTORS\n----------------\n ");

            for (int i = 0; i < NCon; i++)
            {
                con[i].DispCon();
            }


                if (fe[0]==fe[1])
                {
                    Console.WriteLine("\nDesignations Of Employee " + fe[0].Name + " & " + fe[1].Name + "  are Same!");
                }
                else
                    Console.WriteLine("\nDesignations Are Not Same");

                if (fe[0] > fe[1])
                {
                    Console.WriteLine("\nAge Of Employee " + fe[0].Name + " is Greater than age of Employee " + fe[1].Name);
                }
                else
                    Console.WriteLine("\nAge Of Employee " + fe[0].Name + " is Greater than age of Employee " + fe[1].Name);


                fe[0].Check();
                fe[1].Check();
            
            Console.ReadLine();
        }
    }
}
