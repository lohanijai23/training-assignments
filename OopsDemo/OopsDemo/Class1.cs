﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OopsDemo
{
    public static class CheckExp
    {
        public static void Check(this FullTimeEmp e)
        {
            if (e.exp<=5)
            {
                Console.WriteLine("Employee " + e.Name + "'s Experience is less than 5 years");
            }
            else
                Console.WriteLine("Employee " + e.Name + "'s Experience is more  than 5 years");
        }
    }
}
