﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OopsDemo
{
    public class FullTimeEmp: Person
    {
        string Desig;
        float Salary;
      public  int exp;

       public void GetEmp()
        {
            Console.WriteLine("\nEnter Designation of Employee: ");
            Desig = Console.ReadLine();

            
            Console.WriteLine("Enter Salary of Employee: ");
            Salary = float.Parse(Console.ReadLine());

            Console.WriteLine("Enter Experience of Employee: ");
            exp = int.Parse(Console.ReadLine());


        }

        public void DispEmp()
        {
            Console.WriteLine("\nName : "+ Name);
            Console.WriteLine("Age: "+ Age);
            Console.WriteLine("Designation : "+Desig);
            Console.WriteLine("Salary: "+Salary);
            Console.WriteLine("Experience: " + exp);
        }

        public static bool operator== (FullTimeEmp emp1, FullTimeEmp emp2)
        {
            if (emp1.Desig == emp2.Desig)
                return true;
            return false;
        }

        public static bool operator !=(FullTimeEmp emp1, FullTimeEmp emp2)
        {
            if (emp1.Desig != emp2.Desig)
                return true;
            return false;
        }

        public static bool operator >(FullTimeEmp emp1, FullTimeEmp emp2)
        {
            if (emp1.Age >=emp2.Age)
                return true;
            return false;
        }

        public static bool operator <(FullTimeEmp emp1, FullTimeEmp emp2)
        {
            if (emp1.Age < emp2.Age)
                return true;
            return false;
        }
    }
}
