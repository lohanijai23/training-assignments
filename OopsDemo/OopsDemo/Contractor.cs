﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OopsDemo
{
    class Contractor: Person
    {
        int Id;
        float Rate;

       public void GetCon()
        {
            Console.WriteLine("\nEnter Contractor ID: ");
            Id = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter Rate:  ");
            Rate = float.Parse(Console.ReadLine());

        }

        public void DispCon()
        {
            Console.WriteLine("\nName : "+Name);
            Console.WriteLine("Age: "+Age);
            Console.WriteLine("Contractor ID: "+ Id);
            Console.WriteLine("Contractor Rate: "+ Rate);
        }
    }
}
