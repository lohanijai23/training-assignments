﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CustomerMgmtBL
{
    public class Customer:Order
    {
        private string _lastName;
        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
            }

        }

        public string FirstName { get; set; }
        public string EmailId { get; set; }
        public int CustomerId { get; private set; }

        public static int CCount = 1;

        public Customer()
        {

        }

        public Customer(string fn, string ln, string eid)
        {
            FirstName = fn;
            LastName = ln;
            EmailId = eid;
            CustomerId = CCount;
            CCount++;
        }


        public string FullName {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        public static Customer CreateCustomer()
        {
            Console.WriteLine("Enter First Name: ");
            string fn = Console.ReadLine();

            Console.WriteLine("Enter Last Name: ");
            string ln = Console.ReadLine();

            Console.WriteLine("Enter Email Id: ");
            string eid = Console.ReadLine();

            Customer obj = new Customer(fn, ln, eid);
           
            return obj;
        }

        public void ShowCustomers()
        {
            Console.WriteLine("\nCustomer ID:  "+CustomerId);

            Console.WriteLine("Customer Name: "+FullName);
            

            Console.WriteLine("Customer Email Id: "+EmailId);

        }

        public static bool Validate( List<Customer> CList, string fn, string em)
        {
            for(int i=0; i<CList.Count();i++)
            {
                if (CList[i].FirstName == fn && CList[i].EmailId == em)
                    return true;
            }
            return false;
        }
    }
}
