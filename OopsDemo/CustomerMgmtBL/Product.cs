﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerMgmtBL
{
   public class Product
    {
        

        public int ProdId { get; set; }
        public float Price { get; set; }
        public string ProdName { get; set; }
        public string Description { get; set; }

        public Product()
        {
            ProdId = -1;
            Price = -1;
            ProdName = "";
            Description = "";

        }

        public Product(int id, float p, string n, string d)
        {
            ProdId = id;
            Price = p;
            ProdName = n;
            Description = d;

        }

        public static Product AddToCatalog()
        {
            Random r = new Random();
            string name="Item ";

            int n = r.Next(10, 50);
            name = name + n;

            string desc = "Information about Item "+n;

            int id = r.Next(1000 ,1999);

            float p = r.Next(500 ,10000);

            Product prod = new Product(id, p, name, desc);

            return prod;
        }

        public void ShowProducts()
        {
            Console.WriteLine("\n\nProduct ID: "+ProdId);
            Console.WriteLine("Product Name: " + ProdName);
            Console.WriteLine("Description: " + Description);
            Console.WriteLine("Price: "+Price);

        }

    }


}
