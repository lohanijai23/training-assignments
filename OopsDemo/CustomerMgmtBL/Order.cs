﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerMgmtBL
{
   public class Order: Product
    {
        public string OrderDate { get; set; }
        public int OrderId { get; set; }
        public int Quantity { get; set; }
        public float OrderPrice { get; set; }

        public Order(): base()
        {
            OrderDate = "";
            OrderId = -1;
            Quantity = -1;
            OrderPrice = -1;

        }

        public Order(int id, float p, string n, string d, string dt, int oid, int q, float op) :base( id, p, n, d)
        {
            
            OrderDate = dt;
            OrderId = oid;
            Quantity = q;
            OrderPrice = op;
        }

        public static Order CreateOrder(List<Product> p)
        {
          

            Console.WriteLine("\n\nEnter Product ID: ");
            int id = int.Parse(Console.ReadLine());
            float price = 0;
            string name = "";
            string desc = "";
            for (int i = 0; i < p.Count; i++)
            {
                if (p[i].ProdId == id)
                {
                    name = p[i].ProdName;
                     price = p[i].Price;
                    desc = p[i].Description;
                }

            }

            Console.WriteLine("Enter Quantity: ");
            int q = int.Parse(Console.ReadLine());


            Random r = new Random();
            int oid = r.Next(5000 , 5999);
            Order o = new Order(id,price,name,desc, "25/08/2018", oid, q, q*price);
            return o;
        }

        public void ShowOrders()
        {
            Console.WriteLine( "\nOrder ID: "+ OrderId);
            Console.WriteLine("Order Date: " + OrderDate);
            Console.WriteLine("Product ID: " + ProdId);
            Console.WriteLine("Product Name: " + ProdName);
            Console.WriteLine("Description: " + Description);
            Console.WriteLine("Price: " + Price);
            Console.WriteLine("Quantity: " + Quantity);
            Console.WriteLine("Order Total: " + OrderPrice);

          
          
        }
    }

  
}
