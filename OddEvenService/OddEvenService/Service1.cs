﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;

namespace OddEvenService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    [ServiceBehavior(InstanceContextMode= InstanceContextMode.PerCall, ConcurrencyMode =ConcurrencyMode.Multiple)]
    public class Service1 : IService1
    {

        int i = 0;

        public int Increment()
        {
            i++;
            return i;
        }

        public string Call()
        {
            Thread.Sleep(5000);
            i++;

            return string.Format("Instance: "+i+" Thread: "+ Thread.CurrentThread.ManagedThreadId);
            
        }

        public string GetData(int value)
        {
            if(value%2==0)
                return "You entered even number";
            else
                return "You entered odd number";
        }

        public void OneWay()
        {
            Thread.Sleep(5000);
            return;
            
        }

        public string RequestReply()
        {
            DateTime d = DateTime.Now;
            Thread.Sleep(5000);

            DateTime d2 = DateTime.Now;

            return d2.Subtract(d).Seconds.ToString()+ " seconds";

        }
    }
}
