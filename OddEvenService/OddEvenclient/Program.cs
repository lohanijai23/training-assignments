﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddEvenclient
{
    class Program
    {
        static void Main(string[] args)
        {

            ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();

            Console.WriteLine("Enter a number");
            int n = int.Parse(Console.ReadLine());

            string s = client.GetData(n);
            Console.WriteLine(s);

            DateTime d = DateTime.Now;
            Console.WriteLine("Request sent at Time : "+d);

            string s2 = client.RequestReply();
            d = DateTime.Now;
            Console.WriteLine("Response received at: "+d+ ". Time Taken: "+s2);




            d = DateTime.Now;
            Console.WriteLine("One Way Request sent at Time : " + d);
            client.OneWay();
            d = DateTime.Now;
            Console.WriteLine("One Way Request Response received at Time : " + d);



            Console.ReadLine();
        }

    }
}
