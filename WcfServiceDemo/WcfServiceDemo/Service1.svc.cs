﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfServiceDemo
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IStudent
    {
        public bool AddStudent(Student student)
        {
            List<Student> st = new List<Student>();
            st.Add(new Student { Id = "1",Name="Jai" });
            st.Add(new Student { Id = "2", Name = "rao" });
            st.Add(new Student { Id = "3", Name = "raj" });

            return true;
        }

        public List<Student> GetAllStudent()
        {
            List<Student> st = new List<Student>();
            st.Add(new Student { Id = "1", Name = "Jai" });
            st.Add(new Student { Id = "2", Name = "rao" });
            st.Add(new Student { Id = "3", Name = "raj" });

            return st;
        }

     

        public Student GetStudentById(string id)
        {
            switch (id)
            {
                case "1":
                    return new Student { Id = "1", Name = "Jai" };
                    
                case "2":
                    return new Student { Id = "1", Name = "Jai" };
                    

                case "3":
                    return new Student { Id = "1", Name = "Jai" };

                default:
                    return null;


            }
        }

        
    }
}
