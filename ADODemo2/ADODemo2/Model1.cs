namespace ADODemo2
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Collections.Generic;

    public class Model1 : DbContext
    {
        
        public DbSet<Departments> Departments { get; set; }
        // Your context has been configured to use a 'Model1' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'ADODemo2.Model1' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'Model1' 
        // connection string in the application configuration file.
        public Model1()
            
        {
            Database.SetInitializer(new Model1Initializer());
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    [Table("tblDepartments")]
    public class Departments
    {
        [Key]

        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
    }


    //public class SampleRepo
    //{
    //    Model1 context = new Model1();
    //    public IEnumerable <Departments> GetDepartments()
    //    {
    //        return context.Departments.ToList();
    //    }
    //}
}