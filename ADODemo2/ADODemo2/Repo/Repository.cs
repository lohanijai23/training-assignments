﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADODemo2.Repo
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private Model1 sampleDbContext;
        private DbSet<T> dbSet;

        public Repository()
        {
            sampleDbContext = new Model1();
            dbSet = sampleDbContext.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
            return dbSet.ToList();
         
        }

        public void Save()
        {
            sampleDbContext.SaveChanges();
        }
    }
}
