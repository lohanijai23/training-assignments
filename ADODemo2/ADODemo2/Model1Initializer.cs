﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ADODemo2
{
    public class Model1Initializer : DropCreateDatabaseAlways<Model1>
    {

        private List<Departments> departments;

        protected override void Seed(Model1 context)
        {
            departments= new List<Departments> 
            {
                new Departments(){  Location= "Pune", Name="Jai"},
                new Departments(){  Location= "Mumbai", Name="Rao"},

            };
            context.Departments.AddRange(departments);
            base.Seed(context);
        }
    }
}
